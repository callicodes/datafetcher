import React from "react"
import DataFetcher from "./DataFetcher"

function App() {
  return (
    <div>
      <DataFetcher url="https://itunes.apple.com/lookup?upc=720642462928&entity=song">
        {({ data, loading }) => (

          loading ?
            <h1>Loading...</h1> :
            <p>{JSON.stringify(data)}</p>

        )}

      </DataFetcher>
    </div>

  )

}

export default App